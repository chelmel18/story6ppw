function bookSearch(){
    var keyword = document.getElementById('search').value
    document.getElementById("results").innerHTML =""
    console.log(keyword)

    
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + keyword,
        dataType: "json",

        success : function(data){
            for (i = 0; i< data.items.length; i++){
                results.innerHTML += bookDetail(data.items[i], i+1);
            }       
        },
        type : 'GET',
    })
    
}

function bookDetail(book, no){
    //url : "https://www.googleapis.com/books/v1/volumes/"+ book.id
    return (
        "<tr>"+
                "<th scope='row'>"+no+"</th>"+
                    "<td>"+ book.volumeInfo.title +"</td>"+
                    "<td>"+ book.volumeInfo.publisher +"</td>"+
                    "<td>"+ book.volumeInfo.publishedDate +"</td>"+
                    "<td> <img src="+ book.volumeInfo.imageLinks.smallThumbnail +"></td>"+
        "<tr>"
    )
}


  
document.getElementById('button').addEventListener('click', bookSearch, false)
document.getElementById('search').addEventListener('keypress', bookSearch, false)