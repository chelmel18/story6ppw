from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import statusForm
from .models import Status

# Create your views here.

#def homepage(request):
	#if request.method == 'POST':
		#formstatus = statusForm(request.POST)
		#if formstatus.is_valid():

			#status = Status(status = formstatus.data['status'])
			#status.save()
			#return redirect('homepage:myhomepage')
	#else:
		#formstatus = statusForm()

	#status_urut = Status.objects.order_by("tanggal")
	#response = {
		#'form' : formstatus,
		#'daftar_status' : status_urut,
	#}
	#return render(request, 'homepage.html', response)
	
def homepage(request):
	form = statusForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		stat = Status(status = form.data['status'])
		stat.save()
	iniform = statusForm()
	urutan = Status.objects.order_by('-tanggal')
	return render(request, 'homepage.html', {'form': iniform, 'daftar_status': urutan})
