from django.test import TestCase, Client, LiveServerTestCase
from django.utils import timezone
from django.urls import resolve
from homepage.views import homepage
from .models import Status
from .forms import statusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story_6_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_tidak_ada_url(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 404)

    def test_story_6_pake_func_views(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_story_6_pake_base(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_ada_halo(self):
        response = Client().get('')
        content = response.content.decode("utf8")
        self.assertIn("Halo, Apa Kabar?", content)

    def test_model_status_bisa_buat_status(self):
        status_baru = Status.objects.create(status="test status", tanggal=timezone.now())
        hitungStatus = Status.objects.all().count()
        self.assertEqual(hitungStatus, 1)

    def test_input_form_statusForm(self):
        response = Client().post('', data={'status':"test", 'tanggal':timezone.now()})
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn("test", content)

    def test_form_ada_statusForm(self):
        response = Client().get('')
        content = response.content.decode("utf8")
        case1 = "<p>Status:"
        case2 = "<form"
        self.assertIn(case2, content)
    
class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_memasukan_status(self):
        self.selenium.get(self.live_server_url)
        status = self.selenium.find_element_by_id('id_status')
        submit = self.selenium.find_element_by_id('id-submit')

        # Fill the form with data
        status.send_keys('Ini statusku')

        submit.click()
        time.sleep(10)
