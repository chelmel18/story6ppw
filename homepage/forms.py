from django import forms
from . import models
import datetime

class statusForm(forms.Form):
    status = forms.CharField(
        widget= forms.Textarea(
            attrs= {
                "class" : "txt-form form-control status-form",
                "placeholder" : "Tulis status kamu disini",
                "required"  : True,
                "rows":5, 
                "cols":10,
            }
        )
    )
    
    class Meta:
        model = models.Status
        fields = [
            'status', 'tanggal',
        ]