# Generated by Django 2.2.7 on 2019-11-04 23:29

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=300)),
                ('tanggal', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
