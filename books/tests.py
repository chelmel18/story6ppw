from django.test import TestCase, Client
from django.utils import timezone
from django.urls import resolve
from books.views import books
from django.http import HttpRequest

# Create your tests here.
class Story8BooksUnitTest(TestCase):
    def test_story_8_books_ada_url(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_story_8_tidak_ada_url(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 404)

    def test_story_8_pake_func_views(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_story_8_pake_base(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

