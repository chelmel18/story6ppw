from django.test import TestCase, Client
from django.utils import timezone
from django.urls import resolve
from login.views import loginpage
from django.http import HttpRequest

# Create your tests here.
class Story9LoginUnitTest(TestCase):
    def test_story_9_login_ada_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_tidak_ada_url(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 404)

    def test_story_9_pake_func_views(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginpage)

    def test_story_9_pake_base(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')