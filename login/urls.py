from django.urls import path
from . import views

app_name = 'login'

urlpatterns = [
    path('', views.loginpage, name='myloginpage'),
    path('logedin/', views.logedin, name="mylogedin"),
    path('logout/', views.log_out, name='mylogout')

]