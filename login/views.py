from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def loginpage(req):
    error = "Username or Password is wrong"
    user = req.user
    if user.is_authenticated:
        return redirect('login:mylogedin')
    if req.method == 'POST':
        form = AuthenticationForm(data=req.POST)
        if form.is_valid():
            user = form.get_user()
            login(req, user)
            return redirect('login:mylogedin')
    else:
        form = AuthenticationForm()
    return render(req, 'login.html', {'form':form, 'error':error})

def logedin(req):
    return render(req, 'logedin.html')

def log_out(req):
    logout(req)
    return redirect('login:myloginpage')
