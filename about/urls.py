from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'about'

urlpatterns = [
    path('', views.about, name='myabout'),
  
]
