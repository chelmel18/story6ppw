from django.test import TestCase, Client
from django.utils import timezone
from django.urls import resolve
from about.views import about
from django.http import HttpRequest

# Create your tests here.
class Story7AboutUnitTest(TestCase):
    def test_story_7_about_ada_url(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_story_7_tidak_ada_url(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 404)

    def test_story_7_pake_func_views(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_story_7_pake_base(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')